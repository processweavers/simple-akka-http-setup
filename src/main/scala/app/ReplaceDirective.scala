package app

import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.directives._
import akka.stream.scaladsl.Framing
import akka.util.ByteString

trait ReplaceDirective extends BasicDirectives {

  def replacePlaceHolder(s: String, replacements: Map[String, String]): String =
    replacements.foldLeft(s) { case (t, (k, v)) => t.replace(s"$$$$$k$$$$", v) }

  def replace(m: Map[String, String]): Directive0 =
    mapResponseEntity {
      case entity @ HttpEntity.Default(ct, length, data) =>
        val replaced = data
          .via(Framing.delimiter(ByteString("\n"), Int.MaxValue))
          .map(line => ByteString(replacePlaceHolder(line.utf8String, m)))
          .intersperse(ByteString("\n"))
        HttpEntity.CloseDelimited(ct, replaced)
      case entity @ HttpEntity.Strict(ct, data) =>
        HttpEntity.Strict(ct, ByteString(replacePlaceHolder(data.utf8String, m)))
      case entity => throw new RuntimeException(s"Invalid http entity type $entity")
    }

}
