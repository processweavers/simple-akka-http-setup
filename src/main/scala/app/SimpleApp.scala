package app

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

import scala.util.Properties

object SimpleApp {

  implicit val system = ActorSystem("simple-app")
  implicit val materializer = ActorMaterializer()

  def main(args: Array[String]): Unit = {
    system.log.info("Starting web server on {}:{}", "0.0.0.0", 8080)
    Http().bindAndHandle(SimpleRoute.route, "0.0.0.0", 8080)
  }
}

object SimpleRoute extends ReplaceDirective {
  def token(x: String) = x -> fromEnv(x)
  def fromEnv(n: String) = Properties.envOrElse(n, s"{$n not resolved}")
  val assetPrefix = "assets"
  val route: Route = {
    // format: OFF
    pathEndOrSingleSlash {
      redirect("/assets/index.html", StatusCodes.MovedPermanently)
    } ~
    path("ping") {
      complete((StatusCodes.OK, "pong"))
    } ~
    pathPrefix(assetPrefix) {
      get {
        replace(Map(token("X_NAME"))) {
          getFromResourceDirectory(assetPrefix)
        }
      }
    }
    // format: ON
  }
}