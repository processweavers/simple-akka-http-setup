package app

import akka.http.scaladsl.model.StatusCodes
import org.scalatest.{ Matchers, WordSpec }
import akka.http.scaladsl.testkit.ScalatestRouteTest

class SimpleRouteTest extends WordSpec with Matchers with ScalatestRouteTest {

  "The SimpleRoute" must {
    "respond to '/ping' with 200 OK and 'pong'" in {
      Get("/ping") ~> SimpleRoute.route ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] shouldEqual "pong"
      }
    }
  }
}
