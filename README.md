# Simple akka-http setup

## Features
- provides an http server listening on interface 0.0.0.0 and port 8080
- responds to GET on /ping
- demostrates route tests on /ping
- produces a docker container as an artefact (through plugin "sbt-native-packager")
- uses scalariform and sbt-dependency-graph plugins
- serve static HTML content
- configure a replaceable token via env variable
- create and run a docker container 

## Get going
Simply start with  ````sbt run````

Creating a docker container
````
> sbt docker:publishLocal
````
and starting it
````
> docker run -e X_NAME='Bit rocks!' -p 8080:8080 localhost:5000/simple-akka-http-setup:0.0.1-SNAPSHOT
````
Try out the dependency graph plugin with 
````
> sbt dependencyBrowseGraphHtml
[some output]
> open target/graph.html
````
## Further references
- https://doc.akka.io/docs/akka-http/current/routing-dsl/testkit.html
